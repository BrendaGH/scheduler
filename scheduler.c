#include "scheduler.h"

proceso *crear_Task(int _id, char *_funcion){
    proceso *_p = (proceso *)malloc(sizeof(proceso));
    _p->id = _id;
    strcpy(_p->funcion, _funcion);
    return _p;
}

void agregar_Task(proceso *_array, proceso _p, int _index){
    _array[_index] = _p;
}

void ejecutar_Tasks(proceso *_array){
    for(int i = 0; i < NUMTHRDS; i++){
            printf("\t El proceso con ID %d, se encuentra realizando la funcion %s \n", _array[i].id, _array[i].funcion);
            for(int j = 0; j < _array[i].id; j++){
                sleep(1);
            }
        }
    }