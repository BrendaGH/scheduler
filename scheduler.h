#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define UN0			2
#define DOS			1
#define TRES		4
#define CUATRO      3

#define NUMTHRDS    4

typedef struct {
	int id;
	char funcion[100];
    
}proceso;

proceso *crearproceso(int _id, char *_funcion);
void agregarproceso(proceso *_array, proceso _p, int _index);
void ejecutarproceso(proceso *_array);

#endif