#include "scheduler.h"

int main(int argc, char *argv[]){

    proceso *p1 = crearproceso(UN0, "Reproducir pelicula");
    proceso *p2 = crearproceso(DOS, "Abrir LATex");
    proceso *p3 = crearproceso(TRES, "Reproducir musica");
    proceso *p4 = crearproceso(CUATRO, "Abrir navegador");

    proceso *array = (proceso *)malloc(sizeof(proceso)*NUMTHRDS);

    agregarproceso(array, *p1, 0);
    agregarproceso(array, *p2, 1);
    agregarproceso(array, *p3, 2);
    agregarproceso(array, *p4, 3);

    ejecutarproceso(array);

    free(p1);
    free(p2);
    free(p3);
    free(p4);

	return 0;
}